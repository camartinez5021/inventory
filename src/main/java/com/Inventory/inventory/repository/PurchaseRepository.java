package com.Inventory.inventory.repository;

import com.Inventory.inventory.domain.Purchase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PurchaseRepository extends JpaRepository<Purchase, Integer> {

    Optional<Purchase> findByIdPurcharse(Integer idPurcharse);
}
