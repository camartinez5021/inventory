package com.Inventory.inventory.web.rest;

import com.Inventory.inventory.repository.ProductRepository;
import com.Inventory.inventory.service.IProductService;
import com.Inventory.inventory.service.dto.ProductDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;

@RestController
@RequestMapping("/api")
public class ProductResource {

    @Autowired
    IProductService productService;
    @Autowired
    ProductRepository productRepository;

    @PostMapping("/product")
    public ResponseEntity create(@RequestBody ProductDTO productDTO) {
        return productService.create(productDTO);
    }

    @GetMapping("/product")
    public Page<ProductDTO> read(@PathParam("pageSize") Integer pageSize,
                                 @PathParam("pageNumber") Integer pageNumber) {
        return productService.read(pageSize, pageNumber);
    }

    @PutMapping("/product")
    public ProductDTO update(@RequestBody ProductDTO productDTO) {
        return productService.update(productDTO);

    }

    @DeleteMapping("/product/{reference}")
    public void deleteProduct(@PathVariable String refence) {
        productRepository.deleteById(refence);
    }

}
