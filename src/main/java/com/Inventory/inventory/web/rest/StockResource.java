package com.Inventory.inventory.web.rest;

import com.Inventory.inventory.repository.StockRepository;
import com.Inventory.inventory.service.IStockService;
import com.Inventory.inventory.service.dto.StockDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;

@RestController
@RequestMapping("/api")
public class StockResource {

    @Autowired
    IStockService stockService;

    @Autowired
    StockRepository stockRepository;


    @PostMapping("/stock")
    public ResponseEntity create(@RequestBody StockDTO stockDTO) {
        return stockService.create(stockDTO);
    }

    @GetMapping("/stock")
    public Page<StockDTO> read(@PathParam("pageSize") Integer pageSize,
                               @PathParam("pageNumber") Integer pageNumber) {
        return stockService.read(pageSize, pageNumber);
    }

    @PutMapping("/stock")
    public StockDTO update(@RequestBody StockDTO stockDTO) {
        return stockService.update(stockDTO);

    }

    @DeleteMapping("/stock/{id_sotck}")
    public void deleteStock(@PathVariable Integer id_sotck) {
        stockRepository.deleteById(id_sotck);
    }
}





