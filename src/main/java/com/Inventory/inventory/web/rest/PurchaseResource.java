package com.Inventory.inventory.web.rest;

import com.Inventory.inventory.repository.PurchaseRepository;
import com.Inventory.inventory.service.IPurchaseService;
import com.Inventory.inventory.service.dto.PurchaseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;

@RestController
@RequestMapping("/api")
public class PurchaseResource {

    @Autowired
    IPurchaseService purchaseService;

    @Autowired
    PurchaseRepository purchaseRepository;

    @PostMapping("/purchase")
    public ResponseEntity create(@RequestBody PurchaseDTO purchaseDTO) {
        return purchaseService.create(purchaseDTO);
    }

    @GetMapping("/purchase")
    public Page<PurchaseDTO> read(@PathParam("pageSize") Integer pageSize,
                                  @PathParam("pageNumber") Integer pageNumber) {
        return purchaseService.read(pageSize, pageNumber);
    }

    @PutMapping("/purchase")
    public PurchaseDTO update(@RequestBody PurchaseDTO purchaseDTO) {
        return purchaseService.update(purchaseDTO);

    }

    @DeleteMapping("/purchase/{idPurcharse}")
    public void deletePurchase(@PathVariable Integer idPurcharse) {
        purchaseRepository.deleteById(idPurcharse);
    }

}
