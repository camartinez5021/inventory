package com.Inventory.inventory.service;

import com.Inventory.inventory.service.dto.ProductDTO;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

public interface IProductService {

    public ResponseEntity create(ProductDTO productDTO);

    public Page<ProductDTO> read(Integer pageSize, Integer pageNumber);

    public ProductDTO update(ProductDTO productDTO);
}
