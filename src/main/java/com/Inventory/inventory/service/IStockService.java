package com.Inventory.inventory.service;

import com.Inventory.inventory.service.dto.ProductDTO;
import com.Inventory.inventory.service.dto.StockDTO;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

public interface IStockService {

    public ResponseEntity create(StockDTO stockDTO);

    public Page<StockDTO> read(Integer pageSize, Integer pageNumber);

    public StockDTO update(StockDTO stockDTO);
}
