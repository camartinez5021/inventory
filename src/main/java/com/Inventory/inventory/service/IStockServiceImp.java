package com.Inventory.inventory.service;

import com.Inventory.inventory.domain.Stock;
import com.Inventory.inventory.repository.StockRepository;
import com.Inventory.inventory.service.dto.StockDTO;
import com.Inventory.inventory.service.transformer.StockTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class IStockServiceImp implements IStockService {

    @Autowired
    StockRepository stockRepository;

    @Override
    public ResponseEntity create(StockDTO stockDTO) {
        Stock stock = StockTransformer.getStockFromStockDTO(stockDTO);
        if (stockRepository.findByIdSotck(stock.getIdSotck()).isPresent()) {
            return new ResponseEntity("The Stock already exist ", HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity(StockTransformer.getStockDTOFromStock(stockRepository.save(stock)), HttpStatus.OK);
        }
    }

    @Override
    public Page<StockDTO> read(Integer pageSize, Integer pageNumber) {
        Pageable pageable = PageRequest.of(pageSize, pageNumber);
        return stockRepository.findAll(pageable)
                .map(StockTransformer::getStockDTOFromStock);
    }

    @Override
    public StockDTO update(StockDTO stockDTO) {
        Stock stock = StockTransformer.getStockFromStockDTO(stockDTO);
        return (StockTransformer.getStockDTOFromStock(stockRepository.save(stock)));
    }
}
