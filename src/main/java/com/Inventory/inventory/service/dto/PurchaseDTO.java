package com.Inventory.inventory.service.dto;

import com.Inventory.inventory.domain.Product;
import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;


@Setter
@Getter
public class PurchaseDTO {

    @NotNull
    private int idPurcharse;
    private int quantity;
    private int unitPrice;
    private Date datePurchase;
    private Product idProduct;
}
