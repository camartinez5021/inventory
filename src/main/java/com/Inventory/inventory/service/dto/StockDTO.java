package com.Inventory.inventory.service.dto;

import com.Inventory.inventory.domain.Product;
import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
public class StockDTO {

    @NotNull
    private int idSotck;
    private int stock;
    private double total;
    private Product idProduct;
}
