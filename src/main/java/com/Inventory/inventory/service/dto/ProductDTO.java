package com.Inventory.inventory.service.dto;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ProductDTO {

    @NotNull
    private String reference;
    private String nameProduct;
}
