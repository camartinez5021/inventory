package com.Inventory.inventory.service;

import com.Inventory.inventory.domain.Purchase;
import com.Inventory.inventory.repository.PurchaseRepository;
import com.Inventory.inventory.service.dto.PurchaseDTO;
import com.Inventory.inventory.service.transformer.PurcharseTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class IPurchaseServiceImp implements IPurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;

    @Override
    public ResponseEntity create(PurchaseDTO purchaseDTO) {
        Purchase purchase = PurcharseTransformer.getPurchaseFromPurchaseDTO(purchaseDTO);
        if (purchaseRepository.findByIdPurcharse(purchase.getIdPurcharse()).isPresent()) {
            return new ResponseEntity("The Purchase already exist ", HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity(PurcharseTransformer.getPurchaseDTOFroProduct(purchaseRepository.save(purchase)), HttpStatus.OK);
        }
    }

    @Override
    public Page<PurchaseDTO> read(Integer pageSize, Integer pageNumber) {
        Pageable pageable = PageRequest.of(pageSize, pageNumber);
        return purchaseRepository.findAll(pageable)
                .map(PurcharseTransformer::getPurchaseDTOFroProduct);
    }

    @Override
    public PurchaseDTO update(PurchaseDTO purchaseDTO) {
        Purchase purchase = PurcharseTransformer.getPurchaseFromPurchaseDTO(purchaseDTO);
        return (PurcharseTransformer.getPurchaseDTOFroProduct(purchaseRepository.save(purchase)));
    }
}
