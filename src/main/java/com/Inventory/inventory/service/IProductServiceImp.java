package com.Inventory.inventory.service;

import com.Inventory.inventory.domain.Product;
import com.Inventory.inventory.repository.ProductRepository;
import com.Inventory.inventory.service.dto.ProductDTO;
import com.Inventory.inventory.service.transformer.ProductTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class IProductServiceImp implements IProductService {
    @Autowired
    ProductRepository productRepository;


    @Override
    public ResponseEntity create(ProductDTO productDTO) {
        Product product = ProductTransformer.getProductFromProductDTO(productDTO);
        if (productRepository.findByReference(product.getReference()).isPresent()) {
            return new ResponseEntity("The Product already exist ", HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity(ProductTransformer.getProductDTOFromProduct(productRepository.save(product)), HttpStatus.OK);
        }
    }

    @Override
    public Page<ProductDTO> read(Integer pageSize, Integer pageNumber) {
        Pageable pageable = PageRequest.of(pageSize, pageNumber);
        return productRepository.findAll(pageable)
                .map(ProductTransformer::getProductDTOFromProduct);
    }

    @Override
    public ProductDTO update(ProductDTO productDTO) {
        Product product = ProductTransformer.getProductFromProductDTO(productDTO);
        return (ProductTransformer.getProductDTOFromProduct(productRepository.save(product)));
    }
}
