package com.Inventory.inventory.service.transformer;

import com.Inventory.inventory.domain.Product;
import com.Inventory.inventory.service.dto.ProductDTO;

public class ProductTransformer {

    public static ProductDTO getProductDTOFromProduct(Product product) {
        if (product == null) {
            return null;
        }

        ProductDTO dto = new ProductDTO();

        dto.setReference(product.getReference());
        dto.setNameProduct(product.getNameProduct());
        return dto;
    }

    public static Product getProductFromProductDTO(ProductDTO dto) {
        if (dto == null) {
            return null;
        }
        Product product = new Product();

        product.setReference(dto.getReference());
        product.setNameProduct(dto.getNameProduct());
        return product;
    }
}
