package com.Inventory.inventory.service.transformer;

import com.Inventory.inventory.domain.Purchase;
import com.Inventory.inventory.service.dto.PurchaseDTO;

public class PurcharseTransformer {
    public static PurchaseDTO getPurchaseDTOFroProduct(Purchase purchase) {
        if (purchase == null) {
            return null;
        }

        PurchaseDTO dto = new PurchaseDTO();

        dto.setIdPurcharse(purchase.getIdPurcharse());
        dto.setIdProduct(purchase.getIdProduct());
        dto.setQuantity(purchase.getQuantity());
        dto.setUnitPrice(purchase.getUnitPrice());
        dto.setDatePurchase(purchase.getDatePurchase());
        return dto;
    }


    public static Purchase getPurchaseFromPurchaseDTO(PurchaseDTO dto) {
        if (dto == null) {
            return null;
        }

        Purchase purchase = new Purchase();

        purchase.setIdPurcharse(dto.getIdPurcharse());
        purchase.setIdProduct(dto.getIdProduct());
        purchase.setQuantity(dto.getQuantity());
        purchase.setUnitPrice(dto.getUnitPrice());
        purchase.setDatePurchase(dto.getDatePurchase());
        return purchase;
    }
}
