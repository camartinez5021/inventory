package com.Inventory.inventory.service.transformer;

import com.Inventory.inventory.domain.Stock;
import com.Inventory.inventory.service.dto.StockDTO;

public class StockTransformer {
    public static StockDTO getStockDTOFromStock(Stock stock) {
        if (stock == null) {
            return null;
        }

        StockDTO dto = new StockDTO();

        dto.setIdSotck(stock.getIdSotck());
        dto.setIdProduct(stock.getIdProduct());
        dto.setStock(stock.getStock());
        dto.setTotal(stock.getTotal());
        return dto;
    }

    public static Stock getStockFromStockDTO(StockDTO dto) {
        if (dto == null) {
            return null;
        }

        Stock stock = new Stock();

        stock.setIdSotck(dto.getIdSotck());
        stock.setIdProduct(dto.getIdProduct());
        stock.setStock(dto.getStock());
        stock.setTotal(dto.getTotal());
        return stock;
    }
}
