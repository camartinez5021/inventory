package com.Inventory.inventory.service;

import com.Inventory.inventory.service.dto.PurchaseDTO;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

public interface IPurchaseService {

    public ResponseEntity create(PurchaseDTO purchaseDTO);

    public Page<PurchaseDTO> read(Integer pageSize, Integer pageNumber);

    public PurchaseDTO update(PurchaseDTO purchaseDTO);
}
