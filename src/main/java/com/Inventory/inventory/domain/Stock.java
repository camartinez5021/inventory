package com.Inventory.inventory.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Stock {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private int idSotck;


    @Column(name = "stock")
    private int stock;

    @Column(name = "total")
    private double total;

    @ManyToOne(cascade = CascadeType.ALL)
    private Product idProduct;

}

