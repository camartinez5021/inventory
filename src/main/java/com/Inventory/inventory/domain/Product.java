package com.Inventory.inventory.domain;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Product {

    @Id
    @Column(name = "reference", length = 25)
    private String reference;

    @Column(name = "name_product", length = 25)
    private String nameProduct;
}
